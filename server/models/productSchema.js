const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
	id: {
		type: Number,
		required: false
	},
	title: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	desc: {
		type: String,
		required: true
	},
	image: {
		type: String,
		required: false
	},
	stocks: {
		type: Number,
		required: true
	},
	dateAdded: {
	    type: Date,
	    default: new Date()
	}

})

const Product = mongoose.model('product', productSchema);

module.exports = Product;