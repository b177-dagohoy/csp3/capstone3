const mongoose = require('mongoose');


 const subSchema = new mongoose.Schema({
 	email : {
 		type: String,
 		required : true
 	},

    dateAdded: {
        type: Date,
        default: new Date()
    }
 })


 //  Create Model
 const Subscriber = new mongoose.model("SUBSCRIBER", subSchema);

 module.exports = Subscriber;